# BetterPM

BetterPM is a minetest mod that improves private messages. It is completely customizable but also ready to use.

## Config

All these values can be changed in the file `settings.lua`
- `target` represents who receives the message, `sender` represents who sends the message.
- You can customize message text color by changing `targetMsgColor` and `senderMsgColor`.
- You can customize prefix color by changing `targetPrefixColor` and `senderPrefixColor`.
- You can customize the prefix text by changing `targetPrefix` and `senderPrefix`. You can use `{target}` and `{sender}` placeholders to respectively represent the target and the sender names.

## Usage

- `/msg <name> <message>` to send a private message to _name_.
- `/w <name> <message>` as an alias for /msg
- `/r <message>` to reply to the latest private message you received.
