-- use hex codes for colors
-- protip: if you want to use the same color in multiple places you could create
-- a variable and do it like this:
-- local myColor = "#a5cbf8"
-- and then use it where you want this color like this:
-- betterpm.senderPrefixColor = myColor
-- you don't have to use "--" otherwise it will be a comment and won't work
-- if you want a different color for everything you don't need to create variables, just put values
local senderColor = "#d8dee9" -- nord4 from nordtheme.com
local targetColor = "#ebcb8b" -- nord13 from nordtheme.com

betterpm.senderPrefixColor = senderColor
betterpm.targetPrefixColor = targetColor

betterpm.senderMsgColor = senderColor
betterpm.targetMsgColor = targetColor

-- {target} is who receives the message, {sender} is who sends the message
betterpm.senderPrefix = "[You -> {target}] " -- the sender will see this
betterpm.targetPrefix = "[{sender} -> You] " -- the target will receive this


-- if players need to have shout privilege to send private messages
betterpm.requires_shout = true
