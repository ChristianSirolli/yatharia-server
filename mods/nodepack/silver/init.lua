-----------------
-- Ores/blocks --
-----------------

minetest.register_node("silver:silver_block", {
    description = ("Silver Block"),
    tiles = {"silver_block.png"},
    is_ground_content = true,
    groups = {cracky = 3},
})

minetest.register_node("silver:silver_ore", {
	description = ("Silver Ore"),
	tiles = {"default_stone.png^silver_ore.png"},
	is_ground_content = true,
	groups = {cracky = 3},
	drop = "silver:silver_lump",
})

minetest.register_craftitem("silver:silver_ingot", {
	description = ("Silver Ingot"),
	inventory_image = "silver_ingot.png",
})

minetest.register_craftitem("silver:silver_lump", {
	description = ("silver lump"),
	inventory_image = "silver_lump.png",
})

------------
-- Tools --
------------

minetest.register_tool("silver:silver_sword", {
	description = "Silver Sword",
	inventory_image = "silver_sword.png",
	tool_capabilities = {
		full_punch_interval = 0.5,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=1.90, [2]=0.90, [3]=0.30}, uses=50, maxlevel=3},
		},
		damage_groups = {fleshy=8},
	},
	sound = {breaks = "default_tool_breaks"},
	groups = {sword = 1}
})

minetest.register_tool("silver:silver_pickaxe", {
	description = "Silver Pickaxe",
	inventory_image = "silver_pickaxe.png",
	tool_capabilities = {
		full_punch_interval = 0.5,
		max_drop_level=3,
		groupcaps={
			cracky = {times={[1]=2.0, [2]=1.0, [3]=0.50}, uses=80, maxlevel=3},
		},
		damage_groups = {fleshy=5},
	},
	sound = {breaks = "default_tool_breaks"},
	groups = {pickaxe = 1}
})

minetest.register_tool("silver:silver_axe", {
	description = "Silver Axe",
	inventory_image = "silver_axe.png",
	tool_capabilities = {
		full_punch_interval = 0.5,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.10, [2]=0.90, [3]=0.50}, uses=80, maxlevel=3},
		},
		damage_groups = {fleshy=7},
	},
	sound = {breaks = "default_tool_breaks"},
	groups = {axe = 1}
})

minetest.register_tool("silver:silver_shovel", {
	description = "Silver Shovel",
	inventory_image = "silver_shovel.png",
	tool_capabilities = {
         full_punch_interval = 0.5,
	     max_drop_level=1,
		 groupcaps={
			 crumbly = {times={[1]=1.10, [2]=0.50, [3]=0.30}, uses=80, maxlevel=3},
		},
		damage_groups = {fleshy=4},
	},
	sound = {breaks = "default_tool_breaks"},
	groups = {shovel = 1}
})

-----------------
-- Mapgen --
-----------------

-- silver Ore

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "silver:silver_ore",
    wherein        = "default:stone",
    clust_scarcity = 15 * 15 * 15,
    clust_num_ores = 4,
    clust_size     = 3,
    y_max          = -256,
    y_min          = -31000,
})

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "silver:silver_ore",
    wherein        = "default:stone",
    clust_scarcity = 15 * 15 * 15,
    clust_num_ores = 4,
    clust_size     = 3,
    y_max          = -256,
    y_min          = -31000,
})

-------------
-- Crafts --
-------------

-- Tools

minetest.register_craft({
	output = "silver:silver_sword",
	recipe = {
		{"silver:silver_ingot"},
		{"silver:silver_ingot"},
		{"group:stick"},
	}
})

minetest.register_craft({
	output = "silver:silver_pickaxe",
	recipe = {
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
		{"", "group:stick", ""},
		{"", "group:stick", ""},
	}
})

minetest.register_craft({
	output = "silver:silver_shovel",
	recipe = {
		{"silver:silver_ingot"},
		{"group:stick"},
		{"group:stick"},
	}
})

minetest.register_craft({
	output = "silver:silver_axe",
	recipe = {
		{"silver:silver_ingot", "silver:silver_ingot"},
		{"silver:silver_ingot", "group:stick"},
		{"", "group:stick"},
	}
})

minetest.register_craft({
	output = "silver:silver_block",
	recipe = {
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
	}
})

minetest.register_craft({
	output = "silver:silver_ingot 9",
	recipe = {
        {"silver:silver_block"}
	}
})

--
-- Cooking recipes
--

	minetest.register_craft({
		type = "cooking",
		output = "silver:silver_ingot",
		recipe = "silver:silver_lump",
		cooktime = 5,
	})

-- Armor

minetest.register_craft({
	output = "silver:helmet_silver",
	recipe = {
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
		{"silver:silver_ingot", "", "silver:silver_ingot"},
		{"", "", ""},
	}
})

minetest.register_craft({
	output = "silver:chestplate_silver",
	recipe = {
		{"silver:silver_ingot", "", "silver:silver_ingot"},
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
	}
})

minetest.register_craft({
	output = "silver:leggings_silver",
	recipe = {
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
		{"silver:silver_ingot", "", "silver:silver_ingot"},
		{"silver:silver_ingot", "", "silver:silver_ingot"},
	}
})

minetest.register_craft({
	output = "silver:boots_silver",
	recipe = {
		{"silver:silver_ingot", "", "silver:silver_ingot"},
		{"silver:silver_ingot", "", "silver:silver_ingot"},
		{"", "", ""},
	}
})

minetest.register_craft({
	output = "silver:shield_silver",
	recipe = {
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
		{"silver:silver_ingot", "silver:silver_ingot", "silver:silver_ingot"},
		{"", "silver:silver_ingot", ""},
	}
})

-----------------------
-- 3D Armor support --
-----------------------

if minetest.global_exists("3D_armor") then
	armor:register_armor("silver:helmet_silver", {
		description = ("Silver Helmet"),
		inventory_image = "silver_helmet_inv.png",
		groups = {armor_head=1, armor_heal=16, armor_use=70},
		armor_groups = {fleshy=10},
		damage_groups = {cracky=2, snappy=1, level=6},
    })

	armor:register_armor("silver:leggings_silver", {
		description = ("Silver Leggings"),
		inventory_image = "silver_leggings_inv.png",
		groups = {armor_legs=1, armor_heal=16, armor_use=70},
		armor_groups = {fleshy=30},
		damage_groups = {cracky=2, snappy=1, level=6},
	})

	armor:register_armor("silver:chestplate_silver", {
		description = ("Silver Chestplate"),
		inventory_image = "silver_chestplate_inv.png",
		groups = {armor_torso=1, armor_heal=16, armor_use=70},
		armor_groups = {fleshy=30},
		damage_groups = {cracky=2, snappy=1, level=6},
	})

		armor:register_armor("silver:boots_silver", {
		description = ("Silver Boots"),
		inventory_image = "silver_boots_inv.png",
		groups = {armor_feet=1, armor_heal=16, armor_use=70, physics_jump=0.5},
		armor_groups = {fleshy=10},
		damage_groups = {cracky=2, snappy=1, level=6},
	})

		armor:register_armor("silver:shield_silver", {
			description = ("Silver Shield"),
			inventory_image = "silver_shield_inv.png",
			groups = {armor_shield=1, armor_heal=12, armor_use=70},
			armor_groups = {fleshy=10},
			damage_groups = {cracky=2, snappy=1, level=6},
	})

end