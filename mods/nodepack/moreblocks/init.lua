--[[
=====================================================================
** More Blocks **
By Calinou, with the help of ShadowNinja and VanessaE.

Copyright © 2011-2020 Hugo Locurcio and contributors.
Licensed under the zlib license. See LICENSE.md for more information.
=====================================================================
--]]

moreblocks = {
    path = minetest.get_modpath("moreblocks")
}

local modpath = moreblocks.path

moreblocks.S = minetest.get_translator("moreblocks")

dofile(modpath .. "/config.lua")
dofile(modpath .. "/sounds.lua")
dofile(modpath .. "/circular_saw.lua")
dofile(modpath .. "/stairsplus/init.lua")

if minetest.global_exists("default") then
    dofile(modpath .. "/nodes.lua")
    dofile(modpath .. "/redefinitions.lua")
    dofile(modpath .. "/crafting.lua")
    dofile(modpath .. "/aliases.lua")
end
