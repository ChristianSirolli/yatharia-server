WET MUD, 1.0
------------

DESCRIPTION
-----------
This mod adds the Wet Mud node to your game, allowing you to hydrate farmland without using water directly. Hydration works identically to water, 3 nodes around the wet mud.

HOW TO GET
----------
- from the creative inventory
- crafting it by combining dirt with a bucket of water (regular or river water)
- giving it to yourself via command; you first need the "give" privilege: /grantme give
  After you grant yourself that privilege, use this command: /giveme wetmud:wetmud
  If you want more than one, specify a number: /giveme wetmud:wetmud 10

FEEDBACK
--------
You can use the Minetest ContentDB comments to leave feedback, and you can find me in both Minetest and MineClone2 Discord servers if you want to chat about this mod.

I hope you'll find it useful, and thanks for using it. :)

