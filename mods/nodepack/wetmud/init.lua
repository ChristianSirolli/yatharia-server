minetest.register_node("wetmud:wetmud", {
    description = "Hydrates the farmland around it",
    tiles = {"wetmud.png"},
    groups = {water = 1, crumbly = 3}
})

minetest.register_craft({
        type = "shapeless",
        output = "wetmud:wetmud",
        recipe = {
            "bucket:bucket_water", "default:dirt"
        },
        replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}}
})

minetest.register_craft({
        type = "shapeless",
        output = "wetmud:wetmud",
        recipe = {
            "bucket:bucket_river_water", "default:dirt"
        },
        replacements = {{"bucket:bucket_river_water", "bucket:bucket_empty"}}
})

