dungeon_loot = {
    path = minetest.get_modpath("dungeon_loot")
}

dungeon_loot.CHESTS_MIN = 0 -- not necessarily in a single dungeon
dungeon_loot.CHESTS_MAX = 2
dungeon_loot.STACKS_PER_CHEST_MAX = 8

dofile(dungeon_loot.path .. "/loot.lua")
dofile(dungeon_loot.path .. "/mapgen.lua")
