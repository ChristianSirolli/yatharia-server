-- crafting for recipes from nac (minetest4kids)

local default_planks = {
	"default:wood"
}
minetest.register_craft({
	type = "shaped",
	output = "itemshelf:small_shelf",
	recipe = {
		{"default:stick", "group:wood", "default:stick"},
		{"default:stick", "group:wood", "default:stick"},
		{"default:stick", "group:wood", "default:stick"}
	},
})

minetest.register_craft({
	type = "shapeless",
	output = "itemshelf:large_shelf",
	recipe = {
		"itemshelf:small_shelf", 
		"itemshelf:small_shelf", 
		"itemshelf:small_shelf",
	},
})

minetest.register_craft ({
	type = "shaped",
	output = "itemshelf:half_depth_shelf_small",
	recipe = {
		{"", "", ""},
		{"default:stick", "group:wood", "default:stick"},
		{"default:stick", "group:wood", "default:stick"},
	}
})

minetest.register_craft ({
	type = "shapeless",
	output = "itemshelf:half_depth_shelf_large",
	recipe = {
		"itemshelf:half_depth_shelf_small", 
		"itemshelf:half_depth_shelf_small", 
		"itemshelf:half_depth_shelf_small",
	},
})

minetest.register_craft ({
	type = "shaped",
	output = "itemshelf:half_depth_open_shelf_small",
	recipe = {
		{"default:stick", "", "default:stick"},
		{"default:stick", "group:wood", "default:stick"},
		{"default:stick", "", "default:stick"}
	}
})

minetest.register_craft({
	type = "shapeless",
	output = "itemshelf:half_depth_open_shelf_large",
	recipe = {
		"itemshelf:half_depth_open_shelf_small",
		"itemshelf:half_depth_open_shelf_small",
		"itemshelf:half_depth_open_shelf_small"
	}
})
