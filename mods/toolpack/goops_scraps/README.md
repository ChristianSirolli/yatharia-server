# Scraps
Very simple Minetest mod that allows to recycle metal tools

## "If you don't use it, then burn it !"
Put into furnace, the handles of the tools will be burned, but the head of the tool will give scraps/shards/fragments, that can then be put together in order to recover some part of the materials used to craft the tool.

#### Where to find me ?  

I usually play on the Minerland server, and if not online, there is a mailbox at my home (in front of the travelnet cabine)

> Amelaye in Minerland \<amelaye.ddns.net:30000\>  
> More infos : http://minetest.amelieonline.net  
