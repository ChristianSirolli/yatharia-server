spears = {
	path = minetest.get_modpath("spears")
}

dofile(spears.path .. "/defaults.lua")

local input = io.open(spears.path .. "/spears.conf", "r")
if input then
	dofile(spears.path .. "/spears.conf")
	input:close()
	input = nil
end

dofile(spears.path .. "/functions.lua")

dofile(spears.path .. "/tools.lua")

local log_mods = minetest.settings:get_bool("log_mods")

if minetest.settings:get_bool("log_mods") then
	minetest.log("action", "[MOD] Spears loaded")
end
