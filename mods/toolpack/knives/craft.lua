
local items = {
    iron = "default:iron_ingot",
    gold = "default:gold_ingot",
    bronze = "default:bronze_ingot",
    diamond = "default:diamond",
    stick = "group:stick",
    empty = "",
}

minetest.register_craft({
    output = "knives:steel",
    recipe = {
        {items.empty, items.iron},
        {items.stick, items.empty}
    }
})

minetest.register_craft({
    output = "knives:bronze",
    recipe = {
        {items.empty, items.bronze},
        {items.stick, items.empty}
    }
})

minetest.register_craft({
    output = "knives:gold",
    recipe = {
        {items.empty, items.gold},
        {items.stick, items.empty}
    }
})

minetest.register_craft({
    output = "knives:diamond",
    recipe = {
        {items.empty, items.diamond},
        {items.stick, items.empty}
    }
})
