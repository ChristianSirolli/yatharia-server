-- Localize
local crush_nodes = uniham.crush_nodes


-- Register a new hammer
--
-- definition = {
--   name = 'Name of the hammer',
--   head = {
--       texture = 'head_material_texture.png',
--       opacity = 20
--   },
--   texture = 'hammer_texture.png',
--   craft = 'crafting:material',
--   on_use = on_use_function,
--   uses = 123,
-- }
--
-- The `name` will be shown as tooltip on hovering the item in the inventory
-- and will be used as it is.
--
-- `head` is a texture table containing two entries. The `texture` entry
-- defines the texture that is used as base for the hammer head texture mask.
-- The `opacity` entry is the opacity value between 0 (fully transparent) and
-- 255 (fully visible) of the embossing effect. When omitting the opacity
-- value, 51.2 will be used.
--
-- To change the mask textture packs need to add `uniham_head_mask.png` with
-- another file. All black pixels will become transparent. To change the
-- embossing effect add `uniham_emboss.png` with transparent backgground. The
-- embossing texture is simply overlayed over the generated head texture.
--
-- Instead of `head` the entry `texture` can be used. This does not auto-create
-- The hammer texture but uses the given texture file as hammer texture. If
-- both `head` and `texture` are given `texture` gets precedence.
--
-- With `craft` the material to craft the hammer from is defined. Hammers are
-- crafted all the same with the provided material for x and a stick for s
--
--   [ ][x][ ]
--   [ ][s][x]
--   [s][ ][ ]
--
-- If for `craft` a table is provided then the mod tries to register the hammer
-- with the given table as recipe table. The `output` value gets overwritten
-- by the mod but all other values are taken as they are.
--
-- The function provided via `on_use` gets three parameters when called by the
-- hammer’s on_use action: `itemstack`, `player`, and `pointed_thing`. All of
-- those three parameters are defined by Minetest’s API. The function has to
-- modify and return the itemstack as needed.
--
-- If no function is provided the default function (replace nodes by the table
-- provided as `uniham.crush_nodes` in mod loading state).
--
-- `uses` is added as `_uniham.uses` to the hammer’s item definition and can
-- be used by the custom `on_use` function. The built-in function (replacing
-- nodes) calculates the `add_wear` value from this.
--
-- @param short_id   Unprefixed ID for the new hammer
-- @param definition Definition table as described
-- @see <https://dev.minetest.net/on_use#on_use>
-- @see <https://dev.minetest.net/minetest.register_craft>
-- @return void
uniham.register_hammer = function (short_id, definition)
    local on_use = definition.on_use or crush_nodes
    local autotexture = '(([combine:16x16:-1,1=+s)^((+h^+m)^[makealpha:0,0,0))'
    local hammer_id = minetest.get_current_modname()..':'..short_id
    local texture = ''
    local opacity = ''

    if definition.head and not definition.texture then
        texture = autotexture:gsub('+.', {
            ['+s'] = 'default_stick.png',
            ['+h'] = definition.head.texture,
            ['+m'] = 'uniham_head_mask.png'
        })

        opacity = definition.head.opacity or 51.2
        texture = '('..texture..'^(uniham_emboss.png^[opacity:'..opacity..'))'
    end

    if definition.texture then
        texture = definition.texture
    end

    minetest.register_tool(hammer_id, {
        description = definition.name,
        inventory_image = texture,
        _uniham = { uses = definition.uses },
        on_use = function(itemstack, player, pointed_thing)
            return on_use(itemstack, player, pointed_thing)
        end
    })

    if type(definition.craft) == 'string' then
        minetest.register_craft({
            output = hammer_id,
            recipe = {
                { '',            definition.craft,  ''               },
                { '',            'default:stick',   definition.craft },
                { 'default:stick', '',              ''               }
            }
        })
    elseif type(definition.craft) == 'table' then
        definition.craft['output'] = hammer_id
        minetest.register_craft(definition.craft)
    end
end
