-- Register replacements for the hammer. Entry ID is the ID of the node to
-- replace and entry value is the ID of the node to replace with.
local replacements = {
    -- obsidian -> stone -> cobble -> gravel -> sand
    ['default:obsidian']          =  'default:stone',
    ['default:stone']             =  'default:cobble',
    ['default:cobble']            =  'default:gravel',
    ['default:gravel']            =  'default:sand',
    -- sandstone types to sand
    ['default:sandstone']         =  'default:sand',
    ['default:desert_sandstone']  =  'default:desert_sand',
    ['default:silver_sandstone']  =  'default:silver_sand',
    -- desert stone -> desert cobble -> desert sand
    ['default:desert_stone']      =  'default:desert_cobble',
    ['default:desert_cobble']     =  'default:desert_sand',
}


-- Get a node sound
--
-- This helper function returns a sound table or nil depending on wheter the
-- requested sound type exists in the provided node definition or not.
--
-- Common sound types are `dug`, footstep`, or `place`. For example: When
-- requesting the `dug` sounds of a node definition the function checks if
-- the table node_definition.sounds.dug exists and if so it returns that
-- table. If the table does not exist `nil` is returned.
--
-- @param node_definition The desired node’s definition
-- @param sound_type Name of a sound
local get_sound = function (node_definition, sound_type)
    if not node_definition then return end
    if not node_definition.sounds then return end
    if not node_definition.sounds[sound_type] then return end
    return node_definition.sounds[sound_type]
end


-- Hammer use function
--
-- The function is called when using the hammer. It either converts the node to
-- its replacement or does nothing. When converting the modified (added wear)
-- itemstack is returned.
--
-- @param itemstack      The `itemstack` as per on_use definition
-- @param player         The `player` as per on_use definition
-- @param pointed_thing  The `pointed_thing` as per on_use definition
-- @see <https://dev.minetest.net/on_use#on_use>
-- @return itemstack|void
uniham.crush_nodes = function (itemstack, player, pointed_thing)
    if pointed_thing.type ~= 'node' then return end
    local node_pos = minetest.get_pointed_thing_position(pointed_thing)
    local node_name = minetest.get_node(node_pos).name
    local node_sound = get_sound(minetest.registered_nodes[node_name], 'dug')
    local player_name = player:get_player_name()

    if minetest.is_protected(node_pos, player_name) then
        minetest.record_protection_violation(node_pos, player_name)
        return
    end

    if not replacements[node_name] then
        if node_sound then
            minetest.sound_play(node_sound.name, {
                pos = node_pos,
                max_hear_distance = 8,
                gain = node_sound.gain,
            })
        end
        return
    end

    local replacement = replacements[node_name]
    local rep_sound = get_sound(minetest.registered_nodes[replacement], 'dug')
    local uses = minetest.registered_tools[itemstack:get_name()]._uniham.uses

    if rep_sound then
        minetest.sound_play(rep_sound.name, {
            pos = node_pos,
            max_hear_distance = 8,
            gain = rep_sound.gain,
        })
    end

    minetest.set_node(node_pos, { name = replacement })
    minetest.check_for_falling(node_pos)

    itemstack:add_wear(math.ceil(65535 / uses) + 1)
    return itemstack
end
