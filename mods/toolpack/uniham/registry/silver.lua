-- Localize
local register_hammer = uniham.register_hammer
local S = uniham.translator

register_hammer('hammer_silver', {
    name = S('Silver Hammer'),
    head = {
        texture = 'silver_block.png',
    },
    craft = 'silver:silver_ingot',
    uses = 250
})
