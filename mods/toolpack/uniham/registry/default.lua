-- Localize
local register_hammer = uniham.register_hammer
local S = uniham.translator


register_hammer('hammer_bronze', {
    name = S('Bronze Hammer'),
    head = {
        texture = 'default_bronze_block.png',
    },
    craft = 'default:bronze_ingot',
    uses = 150
})

register_hammer('hammer_diamond', {
    name = S('Diamond Hammer'),
    head = {
        texture = 'default_diamond_block.png',
        opacity = 120
    },
    craft = 'default:diamond',
    uses = 300
})

register_hammer('hammer_gold', {
    name = S('Golden Hammer'),
    head = {
        texture = 'default_gold_block.png',
    },
    craft = 'default:gold_ingot',
    uses = 100
})

register_hammer('hammer_mese', {
    name = S('Mese Hammer'),
    head = {
        texture = 'default_mese_block.png',
    },
    craft = 'default:mese_crystal',
    uses = 350
})

register_hammer('hammer_obsidian', {
    name = S('Obsidian Hammer'),
    head = {
        texture = 'default_obsidian_block.png',
        opacity = 25
    },
    craft = 'default:obsidian_shard',
    uses = 400
})

register_hammer('hammer_steel', {
    name = S('Steel Hammer'),
    head = {
        texture = 'default_steel_block.png',
    },
    craft = 'default:steel_ingot',
    uses = 200
})

register_hammer('hammer_stone', {
    name = S('Stone Hammer'),
    head = {
        texture = 'default_stone.png',
    },
    craft = 'default:cobble',
    uses = 70
})

register_hammer('hammer_wood', {
    name = S('Wooden Hammer'),
    head = {
        texture = 'default_wood.png',
    },
    craft = 'default:wood',
    uses = 30
})
