
appliances = {};

local modpath = minetest.get_modpath(minetest.get_current_modname());

appliances.translator = minetest.get_translator("appliances")

appliances.have_mesecons = (minetest.global_exists("mesecons")) or (minetest.global_exists("hades_mesecons"));
appliances.have_pipeworks = minetest.global_exists("pipeworks");
appliances.have_technic = (minetest.global_exists("technic")) or (minetest.global_exists("hades_technic"));

appliances.have_unified = minetest.global_exists("unified_inventory");
appliances.have_craftguide = (minetest.global_exists("craftguide")) or (minetest.global_exists("hades_craftguide2"));
appliances.have_i3 = minetest.global_exists("i3");

appliances.have_tt = minetest.global_exists("tt");

dofile(modpath.."/functions.lua");
dofile(modpath.."/appliance.lua");
dofile(modpath.."/tool.lua");
dofile(modpath.."/extensions.lua");

