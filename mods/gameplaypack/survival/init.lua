survival = {
    path = minetest.get_modpath('survival')
}
--Load File
dofile(survival.path .. '/crafting.lua')
dofile(survival.path .. '/nodes.lua')
dofile(survival.path .. '/tools.lua')
dofile(survival.path .. '/foods.lua')
dofile(survival.path .. '/abms.lua')
dofile(survival.path .. '/craftitem.lua')
dofile(survival.path .. '/functions.lua')
dofile(survival.path .. '/ores.lua')
