

skeletons = {
  path = minetest.get_modpath('skeletons')
};

local modpath = skeletons.path;

skeletons.have_bones = minetest.global_exists("bones") or minetest.global_exists("hades_bones");
skeletons.have_animal = minetest.global_exists("mobs_animal");
if (not skeletons.have_animal) then
  skeletons.have_animal = minetest.global_exists("hades_animals");
end

dofile(modpath.."/functions.lua")

dofile(modpath.."/player/player_skeleton.lua")
if skeletons.have_bones then
  dofile(modpath.."/player/bones.lua")
end
dofile(modpath.."/player/skulls.lua")

dofile(modpath.."/villager/villager_skeleton.lua")

dofile(modpath.."/mobs_animal/bunny_skeleton.lua")
dofile(modpath.."/mobs_animal/chicken_skeleton.lua")
dofile(modpath.."/mobs_animal/cow_skeleton.lua")
dofile(modpath.."/mobs_animal/kitten_skeleton.lua")
dofile(modpath.."/mobs_animal/panda_skeleton.lua")
dofile(modpath.."/mobs_animal/penguin_skeleton.lua")
dofile(modpath.."/mobs_animal/warthog_skeleton.lua")
dofile(modpath.."/mobs_animal/rat_skeleton.lua")
dofile(modpath.."/mobs_animal/sheep_skeleton.lua")

dofile(modpath.."/mob_horse/horse_skeleton.lua")

