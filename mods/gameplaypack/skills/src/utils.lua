function skills.error(pl_name, msg)
	minetest.chat_send_player(pl_name, minetest.colorize("#f47e1b", "[!] " .. msg))
	minetest.sound_play("skills_error", {to_player = pl_name})
end


function skills.print(pl_name, msg)
	minetest.chat_send_player(pl_name, msg)
end


function skills.override_table(original, new)
	local output = table.copy(original)

   for key, new_value in pairs(new) do
		if new_value == "@@nil" then new_value = nil end

		if type(new_value) == "table" and output[key] then
			output[key] = skills.override_table(output[key], new_value)
		else
			output[key] = new_value
		end
   end

	return output
end