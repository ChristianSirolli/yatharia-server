local T = minetest.get_translator("skills")



function skills.start(self, def, args)
	if 
		(not self.loop_params and not def.on_start and not def.on_stop)
		or 
		(self.is_active or not minetest.get_player_by_name(self.pl_name))
	then
		return false
	end

	if not self.data._enabled then
		skills.error(self.pl_name, T("You can't use this skill now!"))
		return false
	end

	if self.cooldown_timer > 0 then
		skills.print_remaining_cooldown_seconds(self)
		return false
	end

	self.is_active = true

	-- Create particle spawners
	if self.attachments.particles then
		self.data._particles = {}

		for i, spawner in ipairs(self.attachments.particles) do
			spawner.attached = self.player
			self.data._particles[i] = minetest.add_particlespawner(spawner)
		end
	end

	-- Create hud
	if self.hud then
		self.data._hud = {}

		for i, hud_element in ipairs(self.hud) do
			local name = hud_element.name
			self.data._hud[name] = self.player:hud_add(hud_element)
		end
	end

	-- Play sounds
	skills.play_sound(self, self.sounds.start, true)
	if self.sounds.bgm then
		self.sounds.bgm.loop = true
		self.data._bgm = skills.play_sound(self, self.sounds.bgm)
	end

	-- Stop skill after duration
	if self.loop_params and self.loop_params.duration then
		minetest.after(self.loop_params.duration, function() self:stop() end)
	end

	-- Attach entities
	if self.attachments.entities then
		for i, entity_def in ipairs(self.attachments.entities) do
			skills.attach_expiring_entity(self, entity_def)
		end
	end

	-- Change sky
	if self.sky then
		local pl = self.player
		self.data._sky = pl:get_sky(true)
		pl:set_sky(self.sky)
	end

	-- Change clouds
	if self.clouds then
		local pl = self.player
		self.data._clouds = pl:get_clouds()
		pl:set_clouds(self.clouds)
	end

	-- Change physics_override
	if self.physics then
		local operation = self.physics.operation -- multiply/divide/add/sub
		
		for property, value in pairs(self.physics) do
			if property ~= "operation" then
				_G["skills"][operation.."_physics"](self.pl_name, property, value)
			end
		end
	end

	self:on_start()
	skills.start_cooldown(self)

	if self.loop_params and not self.loop_params.cast_rate then
		return true
	else
		self:cast(args)
	end

	return true
end