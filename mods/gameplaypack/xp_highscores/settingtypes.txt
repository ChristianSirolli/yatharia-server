# XP in int with that you'll shown on list
xp_to_show (XP to reach the list) int 10000

# How often it update the highscore lists
timer (timer) int 60

#How often the climber-list will reseted in secounds. 86400 = 1 day, 604800 = 1 week.
reset_timer (reset_timer) int 604800

#If xp is over 1000 there stand instead of 1000 1k. At 1000000 1M and above 1000000000 1G
rounding (rounding) bool true
