frostland = {
    path = minetest.get_modpath("frost_land")
}

dofile(frostland.path.."/nodes.lua")
dofile(frostland.path.."/mapgen.lua")
dofile(frostland.path.."/fireflies.lua")
dofile(frostland.path.."/moreblocks.lua")
dofile(frostland.path.."/crafting.lua")
