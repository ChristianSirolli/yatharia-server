prairie = {
    path = minetest.get_modpath("prairie")
}

dofile(prairie.path.."/mapgen.lua")
dofile(prairie.path.."/nodes.lua")
