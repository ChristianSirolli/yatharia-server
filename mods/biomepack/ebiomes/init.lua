ebiomes = {
	path = minetest.get_modpath("ebiomes")
} 

if minetest.settings:get_bool("rereg_mtg_decors", true) then
	dofile(ebiomes.path .. "/reregister.lua")
	dofile(ebiomes.path .. "/reregflowers.lua")
	dofile(ebiomes.path .. "/reregaux.lua")	
end

	dofile(ebiomes.path .. "/biobase.lua")
	dofile(ebiomes.path .. "/decors.lua")
	dofile(ebiomes.path .. "/woods.lua")
	dofile(ebiomes.path .. "/bugs.lua")
	dofile(ebiomes.path .. "/stuff.lua")
	dofile(ebiomes.path .. "/loot.lua")

if minetest.settings:get_bool("reg_hsavanna", true) then	
	dofile(ebiomes.path .. "/savanna.lua")
end

if minetest.settings:get_bool("reg_jprainforest", true) then
	dofile(ebiomes.path .. "/jprainforest.lua")
end
	
if minetest.settings:get_bool("reg_bamboo", true) then
	dofile(ebiomes.path .. "/bamboo.lua")
end

--took a good peek at pigiron mod
if minetest.global_exists("bonemeal") then
	dofile(ebiomes.path .. "/bonemeal.lua")
end

	dofile(ebiomes.path .. "/mobs.lua")

