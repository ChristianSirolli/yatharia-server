--animals
if minetest.global_exists("animalia") and minetest.global_exists("creatura") then

	creatura.register_abm_spawn("animalia:cow", {
		chance = 8000,
		min_height = 5,
		max_height = 200,
		min_group = 3,
		max_group = 4,
		biomes = {'mediterranean', 'warm_steppe', 'steppe', 'cold_steppe', 'deciduous_forest_cold', 'deciduous_forest_warm'},
		nodes = {"ebiomes:dirt_with_grass_med", "ebiomes:dirt_with_grass_steppe_warm", "ebiomes:dirt_with_grass_steppe", "ebiomes:dirt_with_grass_steppe_cold", "ebiomes:dirt_with_grass_cold", "ebiomes:dirt_with_grass_warm"},
		neighbors = {"air", "group:grass", "group:flora"},
	})

	creatura.register_abm_spawn("animalia:sheep", {
		chance = 8000,
		min_height = 0,
		max_height = 200,
		min_group = 3,
		max_group = 6,
		biomes = {'mediterranean', 'warm_steppe', 'steppe', 'deciduous_forest_warm'},
		nodes = {"ebiomes:dirt_with_grass_med", "ebiomes:dirt_with_grass_steppe_warm", "ebiomes:dirt_with_grass_steppe", "ebiomes:dirt_with_grass_warm"},
		neighbors = {"air", "group:grass", "group:flora"},
	})

	creatura.register_abm_spawn("animalia:horse", {
		chance = 16000,
		min_height = 10,
		max_height = 31000,
		min_group = 3,
		max_group = 4,
		biomes = {'mediterranean', 'warm_steppe', 'steppe', 'cold_steppe'},
		nodes = {"ebiomes:dirt_with_grass_med", "ebiomes:dirt_with_grass_steppe_warm", "ebiomes:dirt_with_grass_steppe", "ebiomes:dirt_with_grass_steppe_cold"},
		neighbors = {"air", "group:grass", "group:flora"},
	})
end

--npcs
if minetest.global_exists("homo_sapiens") then
	creatura.register_abm_spawn("homo_sapiens:human", {
		chance = 10000,
		min_height = 0,
		max_height = 31000,
		min_group = 3,
		max_group = 4,
	-- 	biomes = {'mediterranean', 'warm_steppe', 'steppe', 'cold_steppe'},
		nodes = {"default:brick"},
		neighbors = {"ebiomes:grass_med_3", "ebiomes:grass_steppe_warm_3", "ebiomes:grass_steppe_3", "ebiomes:grass_steppe_cold_3", "ebiomes:grass_cold_3", "ebiomes:grass_warm_3", "ebiomes:grass_arid_3", "ebiomes:grass_arid_cool_3"},
	})
end
