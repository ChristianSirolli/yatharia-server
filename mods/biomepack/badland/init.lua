badland = {
    path = minetest.get_modpath("badland")
}

dofile(badland.path.."/mapgen.lua")
dofile(badland.path.."/nodes.lua")
dofile(badland.path.."/flowers.lua")
dofile(badland.path.."/crafting.lua")
dofile(badland.path.."/item.lua")
if minetest.global_exists('moreblocks') then
    dofile(badland.path.."/moreblocks.lua")
end
