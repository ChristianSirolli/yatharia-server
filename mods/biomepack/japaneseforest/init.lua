japaneseforest = {
	path = minetest.get_modpath("japaneseforest")
}
local path = japaneseforest.path
--------------------Biome
	dofile(path .. "/biomes.lua")
	dofile(path .. "/farm.lua")
	dofile(path .. "/mapgen.lua")
	dofile(path .. "/fireflies.lua")
	dofile(path .. "/crafting.lua")
	dofile(path .. "/item.lua")
	dofile(path .. "/nodes.lua")
	dofile(path .. "/tree.lua")
if minetest.global_exists("bonemeal") then
	dofile(path .. "/bonemeal.lua")
end
if minetest.global_exists("moreblocks") then
	dofile(path .. "/moreblocks.lua")
end
-------Bonus
	dofile(path .. "/tools.lua")








