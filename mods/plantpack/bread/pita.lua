-- Load support for MT game translation.
local S = minetest.get_translator("bread")


minetest.register_craftitem("bread:pita", {
    description = S("Pita bread"),
    inventory_image = "bread_pita.png",
    on_use = minetest.item_eat(4),
    groups = {food_bread = 1, flammable =2}
})

minetest.register_craftitem("bread:pita_dough", {
    description = S("Pita dough"),
    inventory_image = "bread_pita_dough.png",
})

minetest.register_craft({
    type = "cooking",
    cooktime = 10,
    output = "bread:pita",
    recipe = "bread:pita_dough",
})

minetest.register_craft({
    output = "bread:pita_dough",
    recipe = {
                {"farming:flour", "farming:glass_water", "farming:flour"}
            },
})

if minetest.global_exists("hunger_ng") then
	hunger_ng.add_hunger_data("bread:pita", { satiates = 6 })
end
