-- Load support for MT game translation.
local S = minetest.get_translator("bread")

minetest.register_craftitem("bread:wholegraincarrotbread", {
    description = S("Wholegrain carrot bread"),
    inventory_image = "bread_wholegraincarrotbread.png",
    on_use = minetest.item_eat(8),
    groups = {food_bread = 1, flammable =2}
})

minetest.register_craftitem("bread:wholegraincarrotbread_dough", {
    description = S("Wholegrain carrot bread dough"),
    inventory_image = "bread_wholegraincarrotbread_dough.png",
})

minetest.register_craft({
    type = "cooking",
    cooktime = 10,
    output = "bread:wholegraincarrotbread",
    recipe = "bread:wholegraincarrotbread_dough",
})

minetest.register_craft({
    output = "bread:wholegraincarrotbread_dough",
    recipe = {
				{"", "farming:sunflower_seeds_toasted", ""},
                {"farming:flour", "farming:glass_water", "farming:carrot"}
            },
})

if minetest.global_exists("hunger_ng") then
	hunger_ng.add_hunger_data("bread:wholegraincarrotbread", { satiates = 8 })
end

