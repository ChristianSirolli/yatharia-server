-- Baking Bread by Marc/Lahusen (C) 2022


bread ={}
bread.modpath = minetest.get_modpath("bread")

dofile(bread.modpath .. "/pita.lua")
dofile(bread.modpath .. "/wholegraincarrotbread.lua")
